import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashRoutingModule } from './dash-routing.module';
import { LayoutsComponent } from './layouts/layouts.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { HomeComponent } from './home/home.component';
import { MesDemandesComponent } from './mes-demandes/mes-demandes.component';
import { NewDemandeComponent } from './new-demande/new-demande.component';
import { HistoryComponent } from './history/history.component';
import { ProfilComponent } from './profil/profil.component';
import { FooterComponent } from './shared/footer/footer.component';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
             //api
import {TableModule} from 'primeng/table';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';

import { FormsModule } from '@angular/forms';
import { AutomobileComponent } from './automobile/automobile.component';
import { ScolaireComponent } from './scolaire/scolaire.component';
import { ImmobilierComponent } from './immobilier/immobilier.component';
import {StepsModule} from 'primeng/steps';
import {FileUploadModule} from 'primeng/fileupload';
import {HttpClientModule} from '@angular/common/http';
import {ToastModule} from 'primeng/toast';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import { ProgressComponent } from './progress/progress.component';

@NgModule({
  declarations: [
    LayoutsComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    MesDemandesComponent,
    NewDemandeComponent,
    HistoryComponent,
    ProfilComponent,
    FooterComponent,
    ScolaireComponent,
    ImmobilierComponent,
    AutomobileComponent,
    ProgressComponent
  ],
  imports: [
    CommonModule,
    DashRoutingModule,
    AccordionModule,
    TableModule,
    CardModule,
    ButtonModule,
    StepsModule,
    FormsModule,
    FileUploadModule,
    HttpClientModule,
    ToastModule,
    MessageModule,
    MessagesModule
  ]
})
export class DashModule { }
