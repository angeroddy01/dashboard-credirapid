import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-demande',
  templateUrl: './new-demande.component.html',
  styleUrls: ['./new-demande.component.css']
})
export class NewDemandeComponent implements OnInit {

  icon1:boolean = false;
  icon2:boolean = false;
  icon3:boolean = false;

  pret :any
  constructor() { }

  ngOnInit(): void {
    this.pret = [
      {
        code:'1', type:"prêt scolaire",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
      {
        code:'2', type:"prêt immobilier",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
      {
        code:'3', type:"prêt automobile",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
    ]
  }

  showIcon1(){
    this.icon1 = true;
    this.icon2 = false;
    this.icon3 = false;
  }

  showIcon2(){
    this.icon1 = false;
    this.icon2 = true;
    this.icon3 = false;
  }
  showIcon3(){
    this.icon1 = false;
    this.icon2 = false;
    this.icon3 = true;
  }
}
