import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';                  //api
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pret :any
  constructor() { }

  ngOnInit(): void {
    this.pret = [
      {
        code:'1', type:"prêt scolaire",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
      {
        code:'2', type:"prêt immobilier",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
      {
        code:'3', type:"prêt automobile",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
    ]
  }

}
