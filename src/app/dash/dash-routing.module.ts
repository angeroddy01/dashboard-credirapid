import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutomobileComponent } from './automobile/automobile.component';

import { HistoryComponent } from './history/history.component';
import { HomeComponent } from './home/home.component';
import { ImmobilierComponent } from './immobilier/immobilier.component';
import { MesDemandesComponent } from './mes-demandes/mes-demandes.component';
import { NewDemandeComponent } from './new-demande/new-demande.component';
import { ProfilComponent } from './profil/profil.component';
import { ProgressComponent } from './progress/progress.component';
import { ScolaireComponent } from './scolaire/scolaire.component';

const routes: Routes = [
  {
    path:'progress',
    component:ProgressComponent
  },
  {
    path:'home',
    component:HomeComponent,
  },
  {
    path:'initier-demande/prêt-scolaire',
    component:ScolaireComponent,
  },
  {
    path:'initier-demande/prêt-immobilier',
    component:ImmobilierComponent,
  },
  {
    path:'initier-demande/prêt-automobile',
    component:AutomobileComponent,
  },
  {
    path:'historique',
    component:HistoryComponent,
  },
  {
    path:'initier-demande',
    component:NewDemandeComponent,
  },
  {
    path:'mes-demandes',
    component: MesDemandesComponent,
  },
  {
    path:'profil',
    component:ProfilComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashRoutingModule { }
