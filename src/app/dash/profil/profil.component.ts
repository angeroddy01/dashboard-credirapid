import { Component, OnInit } from '@angular/core';
import { MenuItem} from 'primeng/api';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  items: MenuItem[] = [];
  activeIndex  = 0;
  constructor() { }

  ngOnInit(): void {
    this.stepperImplement()
  }
    //stepper bar config
    stepperImplement() {
      this.items = [{
        label: 'Client',
        command: (event: any) => {
          this.activeIndex = 0;
        }
      },
      {
        label: 'Employeur',
        command: (event: any) => {
          this.activeIndex = 1;
        }
      },
      {
        label: 'Contrat de prêt',
        command: (event: any) => {
          this.activeIndex = 2;
        }
      },
      {
        label: 'Documents à fournir',
        command: (event: any) => {
          this.activeIndex = 3;
        }
      },
      {
        label: 'Résumé',
        command: (event: any) => {
          this.activeIndex = 4;
        }
      }
      ];
    }


}
