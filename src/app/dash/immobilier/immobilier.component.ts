import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { BackService } from 'src/app/services/back.service';

@Component({
  selector: 'app-immobilier',
  templateUrl: './immobilier.component.html',
  styleUrls: ['./immobilier.component.css'],
  providers: [MessageService]
})
export class ImmobilierComponent implements OnInit {

  uploadedFiles: any[] = [];
  /*FileUpload*/

  /*FileUpload Fin*/
  Montant:any;
  Taux:any;
  annee:any
  number:any;
  object_table:any = []
  cout_credit:any;
  restant_du:any;
  duree_en_mois :any;
  mensualites:any;
  temon:any;
  interets:any;
  capital_amorti:any;

  /*Totaux*/
  totalMontant : any;
  totalCapital:any;
  totalInterets:any;
  items: MenuItem[] = [];
  activeIndex  = 0;
  constructor(private messageService : MessageService,  private backservice: BackService) { }

  ngOnInit(): void {
    this.stepperImplement()
  }

  onUpload(event:any) {
    for(let file of event.files) {
        this.uploadedFiles.push(file);
    }

    this.messageService.add({severity: 'info', summary: 'File Uploaded', detail: ''});
}
  TableauGenerate(){
    this.object_table = [];
    this.Calcul();
    this.Generer();
    console.log(this.object_table);
  }
  Calcul(){
    this.duree_en_mois = (this.annee*12);
    this.Taux = this.Taux / 100;
    this.mensualites = (this.Montant *(this.Taux/12)) / (1- Math.pow((1+(this.Taux/12)), -this.duree_en_mois));
    this.cout_credit = (this.mensualites*this.duree_en_mois) - this.Montant
    /*this.interets = (this.Montant*this.Taux*this.annee) / 100;
    this.capital_amorti = this.mensualites - this.interets
    this.restant_du = this.capital_amorti - this.mensualites*/
    console.log(this.duree_en_mois);
    console.log(this.Taux);
    console.log(this.mensualites);
    console.log(this.cout_credit);
  }

  Generer(){
    this.mensualites = this.mensualites;
    this.cout_credit = this.cout_credit;
    this.annee = this.annee;
    this.Taux = this.Taux;
    this.Montant = this.Montant;
    this.temon = this.Montant;
    for(let i = 1; i<= this.annee*12; i++){
      this.number = i;
      this.interets = Math.round(this.Montant *(this.Taux/12));
      this.capital_amorti = Math.round(this.mensualites - this.interets);
      this.temon = Math.round(this.Montant);
      this.Montant = Math.round(this.Montant - this.capital_amorti);
      if(this.Montant <0){
        this.Montant = 0;
      }
      this.object_table = [...this.object_table,{number:this.number,montant:this.temon,mensualites:this.mensualites,interet:this.interets,amorti:this.capital_amorti,capital:this.Montant}]
    }

  }
  //stepper bar config
  stepperImplement() {
    this.items = [{
      label: 'Client',
      command: (event: any) => {
        this.activeIndex = 0;
      }
    },
    {
      label: 'Employeur',
      command: (event: any) => {
        this.activeIndex = 1;
      }
    },
    {
      label: 'Contrat de prêt',
      command: (event: any) => {
        this.activeIndex = 2;
      }
    },
    {
      label: 'Documents à fournir',
      command: (event: any) => {
        this.activeIndex = 3;
      }
    },
    {
      label: 'Résumé',
      command: (event: any) => {
        this.activeIndex = 4;
      }
    }
    ];
  }

  navigate(action:any){

    if(action=="next"){
      if(this.activeIndex == 4){
        this.activeIndex = 4
      }else{
        this.activeIndex = this.activeIndex + 1
      }
    }else{
      if(this.activeIndex == 0){
        this.activeIndex = 0
      }else{
        this.activeIndex = this.activeIndex - 1
      }
    }
  }

  back(){
    this.backservice.backClicked()
  }
}
