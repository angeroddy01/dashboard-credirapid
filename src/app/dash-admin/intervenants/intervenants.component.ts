import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-intervenants',
  templateUrl: './intervenants.component.html',
  styleUrls: ['./intervenants.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class IntervenantsComponent implements OnInit {
  dialog:boolean = false;
  intervenant :any;
  constructor() { }

  ngOnInit(): void {
    this.intervenant = [
      {
        numero:'1', nom:"N'guessan",prenoms:'Ange Roddy',type:'Niveau 1',
      },
      {
        numero:'2', nom:"N'guessan",prenoms:'Ange Roddy',type:'Niveau 2',
      },
      {
        numero:'3', nom:"N'guessan",prenoms:'Ange Roddy',type:'Niveau 3',
      },
    ]
  }
  openDialog(){
    this.dialog = true
    console.log(this.dialog);

  }
  hideDialog(){
    this.dialog = false
  }
}
