import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashAdminRoutingModule } from './dash-admin-routing.module';
import { PretsComponent } from './prets/prets.component';
import { IntervenantsComponent } from './intervenants/intervenants.component';
import { SuiviDemandesComponent } from './suivi-demandes/suivi-demandes.component';
import { ProfilComponent } from './profil/profil.component';
import { LayoutDashAdminComponent } from './layout-dash-admin/layout-dash-admin.component';
import { SharedComponent } from './shared/shared.component';
import { LeftBarComponent } from './shared/left-bar/left-bar.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { StepsModule } from 'primeng/steps';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'primeng/fileupload';
import { HttpClientModule } from '@angular/common/http';
import { ToastModule } from 'primeng/toast';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import {ToolbarModule} from 'primeng/toolbar';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ProgressBarModule} from 'primeng/progressbar';

@NgModule({
  declarations: [
    PretsComponent,
    IntervenantsComponent,
    SuiviDemandesComponent,
    ProfilComponent,
    LayoutDashAdminComponent,
    SharedComponent,
    LeftBarComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    DashAdminRoutingModule,
    AccordionModule,
    TableModule,
    CardModule,
    ButtonModule,
    StepsModule,
    FormsModule,
    FileUploadModule,
    HttpClientModule,
    ToastModule,
    MessageModule,
    MessagesModule,
    ToolbarModule,
    DialogModule,
    ConfirmDialogModule,
    ProgressBarModule
  ]
})
export class DashAdminModule { }
