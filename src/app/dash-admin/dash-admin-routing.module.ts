import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuiviDemandesComponent } from '../dash-interv/suivi-demandes/suivi-demandes.component';
import { HomeComponent } from './home/home.component';
import { IntervenantsComponent } from './intervenants/intervenants.component';
import { PretsComponent } from './prets/prets.component';
import { ProfilComponent } from './profil/profil.component';

const routes: Routes = [
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'prets',
    component:PretsComponent
  },
  {
    path:'intervenants',
    component:IntervenantsComponent
  },
  {
    path:'profil',
    component:ProfilComponent
  },
  {
    path:'suivi-demandes',
    component:SuiviDemandesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashAdminRoutingModule { }
