import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutDashAdminComponent } from './layout-dash-admin.component';

describe('LayoutDashAdminComponent', () => {
  let component: LayoutDashAdminComponent;
  let fixture: ComponentFixture<LayoutDashAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutDashAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutDashAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
