import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-prets',
  templateUrl: './prets.component.html',
  styleUrls: ['./prets.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class PretsComponent implements OnInit {
  pret:any
  dialog:boolean = false;
  intervenant :any;
  constructor() { }

  ngOnInit(): void {
    this.pret = [
      {
        code:'1',nom_pret:'Prêt immobilier SUPER',date_creation:'25/12/2022',date_modification:'25/12/2022', type:"prêt scolaire",
      },
      {
        code:'2',nom_pret:'Prêt Scolaire meteor',date_creation:'25/12/2022',date_modification:'25/12/2022', type:"prêt immobilier",
      },
      {
        code:'3',nom_pret:'Prêt automobile ',date_creation:'25/12/2022',date_modification:'25/12/2022', type:"prêt automobile",
      },
    ]
  }
  openDialog(){
    this.dialog = true
    console.log(this.dialog);

  }
  hideDialog(){
    this.dialog = false
  }
}
