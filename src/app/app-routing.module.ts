import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ApptestComponent } from './apptest/apptest.component';
import { LayoutDashAdminComponent } from './dash-admin/layout-dash-admin/layout-dash-admin.component';
import { LayoutComponent } from './dash-interv/layout/layout.component';
import { HomeComponent } from './dash/home/home.component';
import { LayoutsComponent } from './dash/layouts/layouts.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {
    path:'login',
    component:LoginComponent
  },
  {
    path: 'dashboard',
    component: LayoutsComponent,
    loadChildren: () => import('./dash/dash.module').then(m => m.DashModule),
  },
  {
    path:'dashboard-intervenant',
    component:LayoutComponent,
    loadChildren: () => import('./dash-interv/dash-interv.module').then(m => m.DashIntervModule),
  },
  {
    path:'dashboard-administrateur',
    component:LayoutDashAdminComponent,
    loadChildren: () => import('./dash-admin/dash-admin.module').then(m => m.DashAdminModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
