import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NouvellesDemandesComponent } from './nouvelles-demandes/nouvelles-demandes.component';
import { ProfilComponent } from './profil/profil.component';
import { SuiviDemandesComponent } from './suivi-demandes/suivi-demandes.component';

const routes: Routes = [
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'profil',
    component:ProfilComponent
  },
  {
    path:'nouvelles-demandes',
    component:NouvellesDemandesComponent
  },
  {
    path:'suivi-demandes',
    component:SuiviDemandesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashIntervRoutingModule { }
