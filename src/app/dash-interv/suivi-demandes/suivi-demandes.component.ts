import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-suivi-demandes',
  templateUrl: './suivi-demandes.component.html',
  styleUrls: ['./suivi-demandes.component.css']
})
export class SuiviDemandesComponent implements OnInit {

  pret :any
  constructor() { }

  ngOnInit(): void {
    this.pret = [
      {
        code:'1',nom_client:'Ange Roddy',date_demande:'25/12/2022', type:"prêt scolaire",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
      {
        code:'2',nom_client:'Ange Roddy',date_demande:'25/12/2022', type:"prêt immobilier",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
      {
        code:'3',nom_client:'Ange Roddy',date_demande:'25/12/2022', type:"prêt automobile",montant:'1000000',duree:'9 mois', statut:'En cours'
      },
    ]
  }
}
