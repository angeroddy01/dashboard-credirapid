import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashIntervRoutingModule } from './dash-interv-routing.module';
import { NouvellesDemandesComponent } from './nouvelles-demandes/nouvelles-demandes.component';
import { SuiviDemandesComponent } from './suivi-demandes/suivi-demandes.component';
import { LayoutComponent } from './layout/layout.component';
import { LeftSidebarComponent } from './shared/left-sidebar/left-sidebar.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { StepsModule } from 'primeng/steps';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'primeng/fileupload';
import { HttpClientModule } from '@angular/common/http';
import { ToastModule } from 'primeng/toast';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import { HomeComponent } from './home/home.component';
import { ProfilComponent } from './profil/profil.component';
@NgModule({
  declarations: [
    NouvellesDemandesComponent,
    SuiviDemandesComponent,
    LayoutComponent,
    LeftSidebarComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    ProfilComponent
  ],
  imports: [
    CommonModule,
    DashIntervRoutingModule,
    AccordionModule,
    TableModule,
    CardModule,
    ButtonModule,
    StepsModule,
    FormsModule,
    FileUploadModule,
    HttpClientModule,
    ToastModule,
    MessageModule,
    MessagesModule,
  
  ]
})
export class DashIntervModule { }
